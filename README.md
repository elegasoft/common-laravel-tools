# common-laravel-tools
A collection of common tools for Laravel projects.


### Installation

Install via composer:
``` 
composer require elegasoft/common-laravel-tools
```

Register the service provider in app/config/app.php:
```
Elegasoft\CommonLaravelTools\ToolServiceProvider::class,
```

Register the exception handlers in app/Exceptions/Handler.php

```
#!php

use Elegasoft\CommonLaravelTools\CommonExceptionHandler;

...

public function render($request, Exception $exception)
    {
        $handler = new CommonExceptionHandler($exception);
        if($handler->catchesException()){
            return $handler->resolve();
        };
        return parent::render($request, $exception);
    }
```

Publish the config and view files:
```
php artisan vendor:publish --provider='Elegasoft\CommonLaravelTools\ToolServiceProvider' 
```

### Alert Messaging 
##### Using alert messaging alone does not require registering the service provider or publishing the config, but the views must be published.

Publish the alert view files (if not published during installation)
```
php artisan vendor:publish --provider='Elegasoft\CommonLaravelTools\ToolServiceProvider' --tag=views
```

Include the alert views to the in your master blade layout file:
```
@include('common-laravel-tools::alerts.all')
```

Dismissable alerts can be thrown from any controller.
```
## Info Alert ##

return redirect()->back()->with('alert-info','Here's an info alert.')

## Success Alert ##

return redirect()->back()->with('alert-success','Here's an success alert!')

## Warning Alert ##

return redirect()->back()->with('alert-warning','Here's an warning alert.')

## Danger Alert ##

return redirect()->back()->with('alert-danger','Here's an danger alert!')
```
## User Authentication