<?php

namespace Elegasoft\CommonLaravelTools\Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Sftp\SftpAdapter;

class DataStore extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that can be updated via forms.
     *
     * @var array
     */
    protected $fillable = ['driver', 'alias', 'host', 'port', 'username', 'password', 'privateKey', 'root', 'timeout', 'directoryPerm'];

    public static function configure_disks()
    {
        self::all()->map(function ($connection) {
            $disk_name = str_slug($connection->alias);
            config(['elfinder.disks.'.$disk_name=>['alias'=>$connection->alias]]);
            config(['filesystems.disks.'.$disk_name=>[
                'driver'=>$connection->driver,
                'host'=>$connection->host,
                'port'=>$connection->port,
                'username'=>$connection->username,
                'password'=>$connection->password,
                'root'=>$connection->root,
            ]]);
        });
    }


}
