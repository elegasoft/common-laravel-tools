<?php

namespace Elegasoft\CommonLaravelTools\Storage;

use Illuminate\Foundation\Http\FormRequest;

class DataStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'driver'=>'required|in:sftp,public,local',
            'host'=>'ip',
            'alias'=>'required|min:5',
            'port'=>'present|numeric',
            'username'=>'required',
            'password'=>'required_without:privateKey,confirmed',
            'privateKey'=>'required_without:password',
            'root'=>'required',


        ];
    }
}
