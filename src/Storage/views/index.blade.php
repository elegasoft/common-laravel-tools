@extends('layouts.app')

@section('content')
    <div class="row-fluid">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a class="btn btn-primary btn-sm pull-right" href="{{ route('connections.create') }}">Add
                        Drive</a>
                    Current Drives
                </div>
                <div class="panel-body">
                    <div class="row-fluid">
                        @foreach($connections as $connection)
                            <div class="row">
                                <div class="col-md-2 col-md-offset-1">
                                    {{ $connection->alias }}
                                </div>
                                <div class="col-md-2">
                                    Host: {{ $connection->host }}
                                </div>
                                <div class="col-md-2">
                                    Port: {{ $connection->port }}
                                </div>
                                <div class="col-md-2">
                                    Username: {{ $connection->username }}
                                </div>
                                <div class="col-md-2">
                                    <form action="{{ route('connections.destroy', $connection->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
