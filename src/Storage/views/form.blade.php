<div class="container-fluid">
    <div class="row-fluid clearfix">
        <form class="form-horizontal" action="{{ $action }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! method_field($method) !!}
            <div class="form-group">
                <label for="driver">Driver</label>
                <select name="driver" class="form-control">
                    <option selected disabled></option>
                    <option value="ftp">FTP Server</option>
                    <option value="sftp">SFTP Server</option>
                </select>
            </div>
            <div class="form-group">
                <label for="host">Host IP Address</label>
                <input name="host" class="form-control" id="host" placeholder="0.0.0.0" value="{{ $datastore->host }}">
            </div>
            <div class="form-group">
                <label for="alias">Name</label>
                <input name="alias" class="form-control" id="alias" placeholder="Business Server" value="{{ $datastore->alias }}">
            </div>
            <div class="form-group">
                <label for="port">Port</label>
                <input name="port" class="form-control" id="port" placeholder="22"  value="{{ $datastore->port }}">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input name="username" class="form-control" id="username" placeholder="Username"  value="{{ $datastore->username }}">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input name="password" class="form-control" id="password" placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password Confirmation</label>
                <input name="password_confirmation" class="form-control" id="password_confirmation"
                       placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="root">Root File Path</label>
                <input name="root" class="form-control" id="root" placeholder="/mnt/array1"  value="{{ $datastore->root }}">
            </div>
            <button type="submit" class="btn btn-primary">{{ $method === "PATCH" ? 'Update' : 'Create' }}</button>
        </form>
    </div>
</div>