@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="btn btn-default btn-sm pull-right" href="{{ route('datastores.index') }}">
                            Return to Connection List
                        </a>
                        Edit Connection
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @include('datastores.form')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
