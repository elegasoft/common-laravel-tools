<?php

namespace Elegasoft\CommonLaravelTools\Storage;

use Elegasoft\CommonLaravelTools\Storage\DataStore;
use Elegasoft\CommonLaravelTools\Storage\DataStoreRequest;
use Illuminate\Http\Request;

class DataStoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datastores = DataStore::all();
        $this->authorize('view', $datastores->first());
        $responsiveView = View::exists('datastores.index') ? 'datastores.index' : 'common-laravel-tools::datastores.index';
        return view($responsiveView, compact('datastores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datastore = new DataStore();
        $this->authorize('create', $datastore);
        $action = route('datastores.store');
        $method = 'POST';
        $responsiveView = View::exists('datastores.create') ? 'datastores.create' : 'common-laravel-tools::datastores.create';
        return view($responsiveView, compact('datastore','action','method'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DataStoreRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DataStoreRequest $request)
    {
        DataStore::create($request->all());
        $responsiveView = View::exists('datastores.index') ? 'datastores.index' : 'common-laravel-tools::datastores.index';
        return redirect()->route($responsiveView)->with('alert-success','Data Store was successfully updated.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datastore = DataStore::findOrFail($id);
        $responsiveView = View::exists('datastores.show') ? 'datastores.show' : 'common-laravel-tools::datastores.show';
        return view($responsiveView, compact('datastore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datastore = DataStore::findOrFail($id);
        $this->authorize('update', $datastore);
        $action = route('datastores.update');
        $method = 'PATCH';
        $responsiveView = View::exists('datastores.edit') ? 'datastores.edit' : 'common-laravel-tools::datastores.edit';
        return view($responsiveView, compact('datastore','action','method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DataStoreRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DataStoreRequest $request, $id)
    {
        $datastore = DataStore::findOrFail($id);
        $datastore->fill($request->all());
        $responsiveView = View::exists('datastores.index') ? 'datastores.index' : 'common-laravel-tools::datastores.index';
        return redirect()->route($responsiveView)->with('alert-success','Data Store was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DataStore::destroy($id);
        $responsiveView = View::exists('datastores.index') ? 'datastores.index' : 'common-laravel-tools::datastores.index';
        return redirect()->route($responsiveView)->with('alert-warning','Data Store was removed and is no longer accessible.');
    }
}
