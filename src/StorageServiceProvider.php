<?php

namespace Elegasoft\CommonLaravelTools;

use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . 'Storage/views' => resource_path('views/vendor/elegasoft/common-laravel-storage')
        ], 'views');
        $this->loadViewsFrom(resource_path('views/vendor/elegasoft/common-laravel-storage'), 'common-laravel-storage');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // create image
        $this->app->singleton('common-exception-handler', function ($app) {
            $e = new \Exception();
            return new CommonExceptionHandler($e);
        });
    }
}
