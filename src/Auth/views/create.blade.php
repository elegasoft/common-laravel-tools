@extends('layouts.app')

@section('content')
    <div class="panel panel-default user">
        <div class="panel-heading">
            <a href="{{ route('user.index') }}" class="btn btn-default pull-right">Return to List</a>
            <h1>Create Admin User</h1>
        </div>
        <div class="panel-body">
            <div class="well-sm">
                @include('form.open')
                @include('admin.user.form')
                @include('form.close')
            </div>
        </div>
    </div>
@endsection