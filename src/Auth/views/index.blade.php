@extends('layouts.app')

@section('content')
    <div class="panel panel-default user">
        <div class="panel-heading">
            @if(auth()->user()->isAdmin())
            <a class="btn btn-success pull-right" href="{{ route('user.create') }}">Add User</a>
            @endif
            <h1>Admin Users</h1>
        </div>
        <div class="panel-body">
            <div class="well-sm clearfix">
                @foreach($users as $user)
                    <div class="clearfix">
                        <div class="col-xs-4">{{ $user->name }}</div>
                        <div class="col-xs-4">{{ $user->email }}</div>
                        <div class="col-xs-4">
                            @if($user->id === auth()->user()->id || auth()->user()->isAdmin())
                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-info pull-left">Edit</a>
                            @endif
                            @if(auth()->user()->isAdmin())
                                <form class="pull-left" action="{{ route('user.destroy', $user->id) }}" method="POST">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection