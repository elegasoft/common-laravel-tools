@extends('layouts.app')

@section('content')
    <div class="panel panel-default user">
        <div class="panel-heading">
            <a href="{{ route('user.index') }}" class="btn btn-default pull-right">Return to List</a>
            <h1>Edit User: {{ $user->name }}</h1>
        </div>
        <div class="panel-body">
            <div class="well-sm">
                @include('form.open')
                <input name="authenticate" value="{{ $user->id }}" type="hidden">
                @include('admin.user.form')
                @include('form.close')
            </div>
        </div>
    </div>
@endsection