<div class="form-group">
    <label>Name</label>
    <input name="name" class="form-control" value="{{ $user->name }}">
</div>
<div class="form-group">
    <label>Email</label>
    <input name="email" class="form-control" value="{{ $user->email }}">
</div>
<div class="form-group">
    <label>Password</label>
    <input name="password" type="password" class="form-control">
</div>
<div class="form-group">
    <label>Confirm</label>
    <input name="password_confirmation" type="password" class="form-control">
</div>