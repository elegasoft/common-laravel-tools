<?php

Route::group(['prefix'=>config('common-laravel-auth.route.prefix'),'middleware'=>config('common-laravel-auth.route.middleware')],function(){
    Route::resource(config('common-laravel-auth.route.name'),Elegasoft\CommonLaravelTools\Auth\AuthController::class);
});
