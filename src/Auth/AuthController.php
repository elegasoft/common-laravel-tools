<?php


namespace Elegasoft\CommonLaravelTools\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @var string
     */
    protected $view_path;
    /**
     * @var string
     */
    protected $route_path;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->view_path = config('common-laravel-auth.view_path', 'common-laravel-auth::users').'.';
        $this->route_path = config('common-laravel-auth.route.name').'.';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view($this->view_path.'index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $action = route('user.store');
        $method = 'POST';
        return view($this->view_path.'create', compact('user','action','method'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        User::create($request->all());
        return redirect()->route($this->route_path.'index')->with('alert-success','User successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view($this->view_path.'edit',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $action = route($this->view_path.'update',$user->id);
        $method = 'PATCH';
        return view($this->view_path.'edit',compact('user','action','method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all())->save();
        return redirect()->route($this->route_path.'index')->with('alert-success','User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route($this->route_path.'index')->with('alert-warn','User was succesffuly deleted.');
    }
}