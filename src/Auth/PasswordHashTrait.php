<?php

namespace Elegasoft\CommonLaravelTools\Auth;


trait PasswordHashTrait
{
    /**
     * Hashes passwords on if they are not already hashed.
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (Hash::needsRehash($value)) {
            $value = Hash::make($value);
        }
        $this->attributes['password'] = $value;
    }
}