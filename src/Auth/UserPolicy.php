<?php

namespace Elegasoft\CommonLaravelTools\Auth;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        $this->policy = config('common-laravel-auth');
    }

    /**
     * Determine whether the user can view the connection.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return call_user_func_array($user,$this->policy['view']);
    }

    /**
     * Determine whether the user can create connections.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return call_user_func_array($user,$this->policy['create']);
    }

    /**
     * Determine whether the user can update the connection.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        return call_user_func_array($user,$this->policy['update']);
    }

    /**
     * Determine whether the user can delete the connection.
     *
     * @param  \App\User  $userk
     * @return mixed
     */
    public function delete(User $user)
    {
        return call_user_func_array($user,$this->policy['delete']);
    }
}
