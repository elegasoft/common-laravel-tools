<?php

namespace Elegasoft\CommonLaravelTools;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/common-laravel-auth.php' => config_path('common-laravel-auth.php')
        ], 'config');
        $this->publishes([
            __DIR__ . '/Auth/views' => resource_path('views/vendor/elegasoft/common-laravel-auth')
        ], 'views');
        $this->loadViewsFrom(resource_path('views/vendor/elegasoft/common-laravel-auth'), 'common-laravel-auth');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
